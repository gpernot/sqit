# About #

`SQuash edIT` is a command line interface to ease writing test cases in [SquashTest](https://www.squashtest.com/),

# Usage #

## `config` ##

Usage: `sqit config URL USER PASSWORD`

Register a new squash instance. This creates a `.sqit` directory at
the current location, and store state files in there.


  * `url`: Squash instance base URL. eg: https://squash.example.com/
  * `user`: Squash instance user login
  * `password`: Squash instance user password

## `pull` ##

Usage: `sqit pull [--filter FILTER]`

Edit a set of test cases. Test cases matching `CASE_FILTER` are
extracted from squash and saved to yaml file.

`CASE_FILTER` is a python-re regular expression matched against test
case names.

  * `--filter CASE_FILTER`: Only edit test cases matching filter.

## `edit` ##

Usage: `sqit edit [--filter FILTER]`

Edit a set of test cases. Test cases matching `FILTER` are
extracted from squash.

`CASE_FILTER` is a python-re regular expression matched against test
case names.

An editor is started to edit test cases, and modifications, if any,
are pushed to server when editor closes.

  * `--filter FILTER`: Only edit test cases matching filter.


## `push` ##

Usage: `sqit push`

Push modifications to Squash server.

# Cheat sheet #

## Squash-in-docker ##

```
docker run -it -p 8090:8080 squashtest/squash-tm
```

## Squash API docuementation ##

Squash API is at https://squash.server/squash/api/rest/latest/docs/api-documentation.html

