"""
Test utils.py
"""

import os.path
from textwrap import dedent
import pytest
import xml.etree.ElementTree as ET
import sqit.utils
from pprint import pprint
from sqit.squash import SqTestCase, SqTestStep


def test_diff_case1():
    orig = dedent('''
      <case id="1" name="test1">
        <description>
        </description>
        <step id="0" index="0">
          <action>
           <p>id0</p>
          </action>
          <expected>
           <p>index0</p>
          </expected>
        </step>
        <step id="1" index="1">
          <action>
           <p>id1</p>
          </action>
          <expected>
           <p>index1</p>
          </expected>
        </step>
      </case>
    ''')
    new = dedent('''
      <case id="1" name="test1">
        <description>
        </description>
        <step id="1">
          <action>
           <p>id1</p>
          </action>
          <expected>
           <p>index0</p>
          </expected>
        </step>
        <step id="0">
          <action>
           <p>id0</p>
          </action>
          <expected>
           <p>index1</p>
          </expected>
        </step>
      </case>
    ''')
    orig_obj = SqTestCase.from_xml(ET.fromstring(orig))
    new_obj = SqTestCase.from_xml(ET.fromstring(new))
    diff, new = orig_obj.diff(new_obj)

    assert diff[0].index == 1 and diff[1].index == 0
    assert new == []


def test_diff_case2():
    orig = dedent('''
      <case id="1" name="test1">
        <description>
        </description>
        <step id="0" index="0">
          <action>
           <p>id0</p>
          </action>
          <expected>
           <p>index0</p>
          </expected>
        </step>
      </case>
    ''')
    new = dedent('''
      <case id="1" name="test1">
        <description>
        </description>
        <step>
          <action>
           New1
          </action>
          <expected>
           <p>index0</p>
          </expected>
        </step>
        <step id="0">
          <action>
           <p>id0</p>
          </action>
          <expected>
           <p>index1</p>
          </expected>
        </step>
        <step>
          <action>
           New2
          </action>
          <expected>
           <p>index2</p>
          </expected>
        </step>
      </case>
    ''')

    orig_obj = SqTestCase.from_xml(ET.fromstring(orig))
    new_obj = SqTestCase.from_xml(ET.fromstring(new))
    diff, new = orig_obj.diff(new_obj)

    print(diff, new)
    assert diff[0].index == 1
    assert len(new) == 2
    for step in new:
        if step.action.text.strip() == 'New1':
            assert step.index == 0
        elif step.action.text.strip() == 'New2':
            assert step.index == 2
        else:
            pytest.fail(f'Unknown action [{step.action.text.strip()}]')

