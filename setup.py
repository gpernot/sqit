from setuptools import setup

setup(
    name="sqit",
    version="0.0.1",
    author="Vivéris Technologies",
    description="Squash TM test cases editor.",
    packages=['sqit'],
    tests_require=['tox'],
    install_requires=['requests', 'click'],
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Scientific/Engineering",
        "Programming Language :: Python",
    ],
    entry_points={
        'console_scripts': [
            'sqit=sqit.cli:cli',
        ],
    },

)
