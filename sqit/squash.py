"""
Squash classes and API
"""

import re
import logging
import xml.etree.ElementTree as ET
import copy
from collections import namedtuple
import requests

from .errors import SqitError, SqitXMLError

logger = logging.getLogger('sqit')


def xml_header(root):
    return f'''<?xml version="1.0"?>
    <!DOCTYPE {root} SYSTEM "/sqit/"
    [<!ENTITY nbsp ' '>]>'''


def _compare_elements(xml1, xml2):
    """Compare XML tree fragments.

    Return False is elements differ.
    """
    if (xml1.tag != xml2.tag or
        xml1.text != xml2.text or
        xml1.tail != xml2.tail or
        xml1.attrib != xml2.attrib):  # noqa: E129
        return False

    if len(xml1) != len(xml2):
        return False

    for iter1, iter2 in zip(xml1, xml2):
        res = _compare_elements(iter1, iter2)
        if not res:
            return False

    return True


def _dump_xml_inner_element(element):
    ET.indent(element)
    res = element.text or ''
    for child in element:
        res += ET.tostring(child,
                           encoding='utf-8', short_empty_elements=False).decode('utf-8')
    res += element.tail
    return res.strip()


TransformRule = namedtuple('TransformRule', ['srctag', 'srcattrs', 'dsttag', 'dstattrs'])


def _transform_element(element, transform_table):
    """Transform squash rich-text editor elements to editable, or the other direction."""

    for elt in element.iter():
        if elt.tag in transform_table:
            transformation = transform_table[elt.tag]
            if elt.attrib == transformation.srcattrs:
                elt.tag = transformation.dsttag
                elt.attrib = transformation.dstattrs


def _richtext_to_editable(element):
    transform_table = {
        'code': TransformRule('code', {},
                              'span', {'style': "font-family:Courier New,Courier,monospace"})
    }
    _transform_element(element, transform_table)


def _editable_to_richtext(element):
    transform_table = {
        'span': TransformRule('span', {'style': "font-family:Courier New,Courier,monospace"},
                              'code', {})
    }
    _transform_element(element, transform_table)


class SqTestStep:
    """Squash test case step"""
    _id: int
    index: int
    action: ET.Element
    expected: ET.Element

    def __init__(self, _id, index, action, expected):
        self.id = int(_id) if _id is not None else None
        self.index = int(index) if index is not None else None
        self.action = action
        self.expected = expected

    @classmethod
    def from_xml(cls, element):
        """Build SqTestStep from an ElementTree element."""
        if element.tag != 'step':
            raise SqitXMLError('XML element is not a `step`')
        _id = element.attrib.get('id')
        index = element.attrib.get('index')
        action = element.find('action')
        _richtext_to_editable(action)
        expected = element.find('expected')
        _richtext_to_editable(expected)
        return cls(_id, index, action, expected)

    def to_xml(self):
        step = ET.Element('step', {'id': str(self.id),
                                   'index': str(self.index)})
        action = copy.deepcopy(self.action)
        _editable_to_richtext(action)
        step.append(action)
        expected = copy.deepcopy(self.expected)
        _editable_to_richtext(expected)
        step.append(expected)
        return step

    def __eq__(self, other):
        return (self.id == other.id and
                self.index == other.index and
                _compare_elements(self.action, other.action) and
                _compare_elements(self.expected, other.expected))

    def __ne__(self, other):
        return not self.__eq__(other)


class SqTestCaseDiff:
    """Differences between two test cases."""
    id: int
    name: str
    path: str
    description: ET.Element
    removed_steps: list[int]
    steps: list[SqTestStep]

    def __init__(self, _id):
        self.id = _id
        self.name = None
        self.description = None
        self.removed_steps = []
        self.steps = []

    def remove_step(self, step_id):
        """Step has been removed."""
        self.removed_steps.append(step_id)

    def add_step(self, step):
        """Step has been added or modified."""
        self.steps.append(step)

    def truth(self):
        """Return True if any changed have been made."""
        return (self.name or self.description or
                self.removed_steps or self.steps)

    def __repr__(self):
        diff = {}
        for field in ['name', 'description', 'removed_steps', 'steps']:
            if getattr(self, field):
                diff[field] = getattr(self, field)
        return str(diff)


class SqTestCase:
    """Squash test case"""
    id: int
    name: str
    path: str
    description: ET.Element
    steps: list[SqTestStep]

    def __init__(self, _id, name, path, description, steps=None):

        self.id = int(_id) if _id is not None else None
        self.name = name
        self.path = path
        self.description = description
        _richtext_to_editable(self.description)
        self.steps = []
        if steps is not None:
            for step in steps:
                assert step['_type'] == 'action-step'
                try:
                    header = xml_header('action')
                    action = ET.fromstring(f"{header}<action>{step['action']}</action>")
                    _richtext_to_editable(action)
                except Exception as exc:
                    raise Exception(f'Error parsing {step["action"]}')

                try:
                    header = xml_header('expected')
                    expected = ET.fromstring(f"{header}<expected>{step['expected_result']}</expected>")
                    _richtext_to_editable(expected)
                except Exception as exc:
                    raise Exception(f'Error parsing {step["expected_result"]}')

                self.steps.append(SqTestStep(step['id'],
                                             step['index'],
                                             action,
                                             expected))
            self.steps = sorted(self.steps, key=lambda s: s.index)

    @classmethod
    def from_xml(cls, element):
        """Build SqTestStep from an ElementTree element."""
        if element.tag != 'case':
            raise SqitXMLError('XML element is not a `case`')
        _id = element.attrib.get('id')
        name = element.attrib.get('name') or ''
        path = element.attrib.get('path') or ''
        description = element.find('description')
        # instantiate new test case object
        test_case = cls(_id, name, path, description)

        for step in element:
            if step.tag == 'description':
                continue
            test_case.steps.append(SqTestStep.from_xml(step))
        return test_case

    def to_xml(self):
        case = ET.Element('case', {'id': str(self.id),
                                   'name': self.name,
                                   'path': self.path})
        description = copy.deepcopy(self.description)
        _editable_to_richtext(description)
        case.append(description)

        for step in self.steps:
            case.append(step.to_xml())
        return case

    def __eq__(self, other):
        """Compare `case` elements.

        nb: enclosed test steps are not compared.
        """
        return (self.id == other.id and
                _compare_elements(self.description, other.description))

    def __ne__(self, other):
        return not self.__eq__(other)

    def lookup_step(self, _id):
        """Get step in with with given `id`."""
        if _id is None:
            return None
        for s in self.steps:
            if s.id == _id:
                return s
        return None

    def diff(self, other):
        """Diff self with another SqTestCase."""

        diff = SqTestCaseDiff(self.id)
        if other.id != self.id:
            raise SqitError("Can't diff cases with different IDs")

        if self.name != other.name:
            diff.name = other.name

        if _dump_xml_inner_element(self.description) != _dump_xml_inner_element(other.description):
            diff.description = other.description

        # re-index steps
        for index, other_step in enumerate(other.steps):
            other_step.index = index
            if getattr(other_step, 'id') is None:
                # create a new step
                other_step.id = None
                diff.add_step(other_step)

        # lookup changes in existing steps
        for self_step in self.steps:
            other_step = other.lookup_step(self_step.id)
            if other_step is None:
                # step has been deleted
                diff.remove_step(self_step.id)
                continue
            if (self_step != other_step):
                diff.add_step(other_step)

        return diff


class Client:

    """Squash client context"""
    def __init__(self, context):
        self.auth = requests.auth.HTTPBasicAuth(context.user,
                                                context.password)
        self.base_url = f"{context.url}/api/rest/latest"

    def squash_api(self, url=None, full_url=None, method='GET', data=None):

        assert url is not None or full_url is not None
        assert not (url is not None and full_url is not None)

        _url = f'{self.base_url}/{url}' if url else full_url

        res = requests.request(method, _url, auth=self.auth,
                               headers={'Accept': 'application/json'},
                               json=data)
        if method in ['GET', 'POST']:
            return res.json()
        return None

    def get_case(self, test_id):
        res = self.squash_api(url=f'test-cases/{test_id}')
        assert res['_type'] == 'test-case'
        header = xml_header('description')
        case = SqTestCase(res['id'], res['name'], res['path'],
                          ET.fromstring(f'{header}<description>{res["description"]}</description>'),
                          res['steps'])
        return case

    def modify_case(self, case):
        logger.debug('Modify case: %r', case)
        data = {'_type': 'test-case'}
        if case.name is not None:
            data['name'] = case.name
        if case.description is not None:
            data['description'] = _dump_xml_inner_element(case.description)
        self.squash_api(url=f'test-cases/{case.id}', method='PATCH', data=data)

    def modify_step(self, step):
        logger.debug('Modify step: %r', step)
        data = {'_type': 'action-step',
                'action': _dump_xml_inner_element(step.action),
                'expected_result': _dump_xml_inner_element(step.expected),
                'index': step.index}
        self.squash_api(url=f'test-steps/{step.id}', method='PATCH', data=data)

    def add_step(self, case_id, step):
        logger.debug('Create new step: %s/%r', case_id, step)
        data = {'_type': 'action-step',
                'action': _dump_xml_inner_element(step.action),
                'expected_result': _dump_xml_inner_element(step.expected),
                'index': step.index}
        self.squash_api(url=f'test-cases/{case_id}/steps', method='POST', data=data)

    def delete_step(self, step_id):
        logger.debug('Delete step: %d', step_id)
        self.squash_api(url=f'test-steps/{step_id}', method='DELETE')

    @staticmethod
    def _filter(filters, json):
        if not filters:
            return True
        for field in ('name', 'path'):
            if filters.get(field):
                if not re.match(filters[field], json[field]):
                    return False
        return True

    def get_cases(self, filters=None, _max=None):
        """Retrieve cases matching filters."""

        size = min(_max, 100) if _max else 100
        res = self.squash_api(url=f'test-cases?size={size}&fields=name,path')
        count = 0
        done = False

        while not done:
            for case in res['_embedded']['test-cases']:
                if self._filter(filters, case):
                    case_obj = self.get_case(test_id=case['id'])
                    yield case_obj

                    if _max is not None:
                        count += 1
                        if count >= _max:
                            done = True
                            break

            if 'next' in res['_links']:
                res = self.squash_api(full_url=res['_links']['next']['href'])
            else:
                break

    def create_user(self, login, password, last_name):
        data = {'_type': 'user',
                'login': login,
                'password': password,
                'last_name': last_name,
                'first_name': '',
                'email': '',
                'group': 'User'
                }
        res = self.squash_api(url='users', method='POST', data=data)
