"""
Squit CLI
"""

import os.path
import shutil
import subprocess
import tempfile
import click
import xml.etree.ElementTree as ET
from pprint import pprint

from .squash import Client
from .confdir import load_config, save_config
from .errors import SqitError
from .utils import diff_xml


config = load_config()


class Context:
    def __init__(self, name=None, url=None, _user=None, password=None,
                 editor=None, filters=None):
        self.name = name
        self.url = url
        self.user = _user
        self.password = password
        self.editor = editor
        self.filters = filters


def _in_repo_filename(name):
    """Get in-repository file name, given instance name"""
    filename = f'{name}.xml'
    return os.path.join('.sqit', filename)


def _push_diff(client, orig_file, new_file):
    """Push differences between orig and new files"""
    diffs = diff_xml(orig_file, new_file)
    print(diffs)
    for case in diffs:

        if case.name or case.description:
            client.modify_case(case)

        for step_id in case.removed_steps:
            # delete step
            client.delete_step(step_id)

        for step in case.steps:
            if step.id is None:
                # create new steps
                client.add_step(case.id, step)
            else:
                # apply modifications
                client.modify_step(step)


def _pull_test_cases(client, ctx, dest_file):
    """Pull test cases to dest_file"""

    # update test cases in repo
    repo_file = _in_repo_filename(ctx.name)
    with open(repo_file, 'w', encoding='utf-8') as f:
        f.write('<sqit>\n')
        # get test cases that match filters
        for case in client.get_cases(filters=ctx.filters):
            xcase = case.to_xml()
            ET.indent(xcase, level=1)
            f.write(ET.tostring(xcase, encoding='utf-8',
                                short_empty_elements=False).decode('utf-8'))
        f.write('\n</sqit>\n')
    shutil.copyfile(repo_file, dest_file)


@click.group('cli')
@click.version_option()
@click.option('--name', help='Squash instance name.',
              default=config['DEFAULT'].get('default'),
              metavar='NAME')
@click.option('--url', help='Base instance URL.')
@click.option('--user', '_user', help='User login.')
@click.option('--password', help='User password.')
@click.option('--editor', help='Editor to use.',
              metavar='EDITOR')
@click.pass_context
def cli(ctx, name, url, _user, password, editor):
    """SquashTest command line interface."""

    if name == 'sqit':
        raise SqitError('''Instance name can't be "sqit"''')
    if not name:
        name = config['DEFAULT'].get('name')
    if name:
        if not url:
            url = config[name]['url']
        if not _user:
            _user = config[name]['user']
        if not password:
            password = config[name]['password']
        if not editor:
            editor = config[name].get('editor',
                                      os.environ.get('EDITOR',
                                                     '/usr/bin/sensible-editor'))

    ctx.obj = Context(name=name, url=url, _user=_user,
                      password=password, editor=editor)


@cli.command('config')
@click.pass_obj
def handle_config(ctx):
    """Configure a squash editor."""

    config['DEFAULT']['editor'] = ctx.editor
    config['DEFAULT']['default'] = ctx.name
    if ctx.name in config:
        for field in ['url', 'user', 'password']:
            if getattr(ctx, field):
                config[ctx.name][field] = getattr(ctx, field)
    else:
        config[ctx.name] = {'url': ctx.url,
                            'user': ctx.user,
                            'password': ctx.password}
    save_config(config)


@cli.command('edit')
@click.option('--name', help='Only edit test cases with name matching `name`',
              metavar='PATTERN')
@click.option('--path', help='Only edit test cases with path matching `path`',
              metavar='PATTERN')
@click.pass_obj
def handle_edit(ctx, name, path):
    """Pull to temporary file for immediate edit."""

    client = Client(ctx)
    ctx.filters = {'name': name, 'path': path}
    repo_file = _in_repo_filename(ctx.name)
    with tempfile.NamedTemporaryFile(suffix='.xml') as edit_fo:
        edit_file = edit_fo.name
        _pull_test_cases(client, ctx, edit_file)

        while True:
            try:
                subprocess.run(ctx.editor.split() + [edit_file], check=True)
            except subprocess.CalledProcessError as exc:
                raise SqitError('Error opening editor') from exc
            try:
                _push_diff(client, repo_file, edit_file)
            except Exception as exc:
                print(f'Error pushing modifications: {exc}')
                res = input('[A]bort / [R]etry ')
                if not res or res[0].lower() == 'r':
                    continue
            break


@cli.command('pull')
@click.option('--name', help='Only pull test cases with name matching `name`',
              metavar='PATTERN')
@click.option('--path', help='Only pull test cases with path matching `path`',
              metavar='PATTERN')
@click.pass_obj
def handle_pull(ctx, name, path):
    """Pull test cases to xml."""

    client = Client(ctx)
    ctx.filters = {'name': name, 'path': path}

    edit_file = f'{ctx.name}.xml'
    repo_file = _in_repo_filename(ctx.name)
    if os.path.exists(edit_file):
        diffs = diff_xml(repo_file, edit_file)
        if diffs:
            raise SqitError(f'There are uncommited changes in {edit_file}')

    _pull_test_cases(client, ctx, edit_file)


@cli.command('push')
@click.pass_obj
def handle_push(ctx):
    """Push modifications to Squash server."""
    client = Client(ctx)

    edit_file = f'{ctx.name}.xml'
    repo_file = _in_repo_filename(ctx.name)
    _push_diff(client, repo_file, edit_file)
    os.remove(edit_file)


@cli.command('diff')
@click.pass_obj
def handle_diff(ctx):
    """Show differences in edit version."""

    edit_file = f'{ctx.name}.xml'
    repo_file = _in_repo_filename(ctx.name)
    if os.path.exists(edit_file):
        diffs = diff_xml(repo_file, edit_file)
        if diffs:
            print(diffs)


@cli.group('user')
def user():
    """Manage users"""


@user.command('create')
@click.argument('login')
@click.argument('password')
@click.argument('last_name')
@click.pass_obj
def user_create(ctx, login, password, last_name):
    """Create a new user."""
    client = Client(ctx)
    client.create_user(login, password, last_name)


if __name__ == '__main__':
    cli()
