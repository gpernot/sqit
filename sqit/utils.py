"""
Various utility functions
"""

import xml.etree.ElementTree as ET
from .squash import SqTestCase


def diff_xml(orig: str, new: str):
    """Return xml fragments of modified test cases and steps between `orig` and `new`."""
    diffs = []

    orig_cases = [SqTestCase.from_xml(case) for case in ET.parse(orig).getroot()]
    new_cases = {}
    for case in ET.parse(new).getroot():
        xcase = SqTestCase.from_xml(case)
        new_cases[xcase.id] = xcase

    for orig_case in orig_cases:
        if orig_case.id in new_cases:
            diff = orig_case.diff(new_cases[orig_case.id])
            if diff:
                diffs.append(diff)

    return diffs
