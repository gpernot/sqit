"""
Configuration directory
"""

import os
import configparser


def load_config():

    os.makedirs('.sqit', mode=0o700, exist_ok=True)

    config = configparser.ConfigParser()
    config.read('.sqit/sqit.ini')

    return config


def save_config(config):

    os.makedirs('.sqit', mode=0o700, exist_ok=True)

    with open('.sqit/sqit.ini', 'w', encoding='utf-8',
              opener=lambda path, flags: os.open(path, flags,
                                                 mode=0o600)) as configfile:
        config.write(configfile)
