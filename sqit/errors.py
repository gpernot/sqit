"""
Squit errors
"""


class SqitError(Exception):
    """Mother of all squit errors"""


class SqitXMLError(Exception):
    """Error parsing XML element"""
